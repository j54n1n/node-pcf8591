var I2C = require('i2c');

var PCF8591 = function(device, slaveAddress, controlByte) {
    this.device = device || '/dev/i2c-1';
    this.slaveAddress = slaveAddress || 0x48;
    this.controlByte = controlByte || 0x00;
    this.i2c = new I2C(this.slaveAddress, {
	device: this.device
    });
    this.configure(controlByte);
};

PCF8591.prototype.configure = function(controlByte, callback) {
    if(controlByte && (this.controlByte !== controlByte)) {
	this.controlByte = controlByte;
	this.write(undefined, callback);
    }
};

PCF8591.prototype.write = function(dataByte, callback) {
    this.i2c.writeBytes(this.controlByte, [dataByte], function(error) {
	if(callback) {
	    callback(error);
	}
    });
};

PCF8591.prototype.read = function(callback) {
    this.readBytes(1, function(error, buffer) {
	callback(error, buffer[0]);
    });
};

PCF8591.prototype.readBytes = function(length, callback) {
    this.i2c.readBytes(this.controlByte, length , function(error, buffer) {
	callback(error, buffer);
    });
};

module.exports = PCF8591;