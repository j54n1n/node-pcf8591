# pcf8591

Thin I2C wrapper to access the PCF8591 8-bit A/D and D/A converter.

## Install

````bash
$ npm install pcf8591
````

## Usage

```javascript
var PCF8591 = require('pcf8591');

// Constructor: PCF8591(device, slaveAddress, controlByte),
// defaults to: '/dev/i2c-1', 0x48, 0x00.
var pcf8591 = new PCF8591();

// Reconfigure control register following the Datasheet.
pcf8591.configure(0x45);

// Discard first reading after POR or read mode change.
pcf8591.read(function(error, sample) {
    console.log('Discarded sample: 0x' + sample.toString(16));
});
pcf8591.readBytes(4, function(error, buffer) {
    console.log('Discarded samples: ' +
                '0x' + buffer[0].toString(16) + ' ' +
                '0x' + buffer[1].toString(16) + ' ' +
                '0x' + buffer[2].toString(16) + ' ' +
                '0x' + buffer[3].toString(16)
               );
});

var output = 0x00;
setInterval(function() {
    // Toggle output.
    output ^= 0xff;
    pcf8591.write(output);
    // Read all channels at once.
    pcf8591.readBytes(4, function(error, buffer) {
        console.log('0x' + buffer[0].toString(16) + ' ' +
                    '0x' + buffer[1].toString(16) + ' ' +
                    '0x' + buffer[2].toString(16) + ' ' +
                    '0x' + buffer[3].toString(16)
                   );
    });
}, 500);
```

## Raspberry Pi Setup

````bash
$ sudo nano /etc/modules
````

Add these two lines:

````bash
i2c-bcm2708 
i2c-dev
````

````bash
$ sudo nano /etc/modprobe.d/raspi-blacklist.conf
````

Comment out the I2C module from the blacklist:

````
#blacklist i2c-bcm2708
````

To make the I2C device writable create a file:

````bash
sudo nano /etc/udev/rules.d/99-i2c.rules
````

and enter:

````
KERNEL=="i2c-1", MODE="1666"
````

Then load kernel module:

````bash
$ sudo modprobe i2c-bcm2708
````
